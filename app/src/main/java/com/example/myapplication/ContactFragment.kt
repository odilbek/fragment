package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myapplication.adapter.ContactAdapter
import com.example.myapplication.data.ContactData

class ContactFragment : Fragment() {


    private lateinit var adapterContacts: ContactAdapter
    private var data = ArrayList<ContactData>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        next.setOnClickListener {
//            Navigation.findNavController(view).navigate(R.id.action_firstFragment_to_secondFragment)
//        }

//        next.setOnClickListener {
//            findNavController().navigate(R.id.action_firstFragment_to_secondFragment)
//        }

    }

}