package com.example.myapplication.data

data class ContactData (
    val name: String,
    val number: String
)