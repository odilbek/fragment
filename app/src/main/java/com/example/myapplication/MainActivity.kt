package com.example.myapplication

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.adapter.ContactAdapter
import com.example.myapplication.data.ContactData
import kotlinx.android.synthetic.main.fragment_contact.*

class MainActivity : AppCompatActivity() {
    private lateinit var adapterContacts: ContactAdapter
    private var data = ArrayList<ContactData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadData()
        adapterContacts = ContactAdapter()
        adapterContacts.loadData(data)
            rvContacts.apply {
            adapter = adapterContacts
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
//        adapterContacts.setOnDeleteClickListener {
//            data.remove(it)
//            adapterContacts.notifyDataSetChanged()
//        }
        adapterContacts.setOnDeleteClickListener { name, number, position ->
            data.removeAt(position)
            adapterContacts.notifyDataSetChanged()
        }
    }

    private fun loadData() {
        data.add(ContactData("Bilol", "999999999"))
        data.add(ContactData("Jasur", "999999999"))
        data.add(ContactData("Odilbek", "999999999"))
        data.add(ContactData("Javohir", "999999999"))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_add, menu)
        return super.onCreateOptionsMenu(menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add_contacts -> {
                data.add(ContactData("asasa", "2323232"))
                adapterContacts.notifyDataSetChanged()
                successDialog().show()
            }
        }
        return true
    }

    private fun successDialog(): AlertDialog.Builder {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Done")
        dialog.setMessage("Successfully Added")
//        dialog.setView(R.layout.dialog_add_contacts)
        dialog.setPositiveButton("Ok") { dialogInterface, i ->

        }
        dialog.setNegativeButton("Cancel") { dialogInterface, i ->

        }
        dialog.create()
        return dialog
    }
}