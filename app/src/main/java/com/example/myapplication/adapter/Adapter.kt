package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.ContactData
import kotlinx.android.synthetic.main.item_contact.view.*

class ContactAdapter : RecyclerView.Adapter<ContactAdapter.ViewHolder>() {
    private var data = ArrayList<ContactData>()
    private var listener: ((String, String, Int) -> Unit)? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_contact, null
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = data[position]
        holder.itemView.apply {
            name.text = contact.name
            number.text = contact.number
            btDelete.setOnClickListener {
                listener?.invoke(contact.name, contact.number, position)
//                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount() = data.size

    fun loadData(data: ArrayList<ContactData>) {
        this.data = data
    }

    fun setOnDeleteClickListener(f: (String, String, Int) -> Unit) {
        listener = f
    }
}